# RFT

## Description

Regex For Type

## Preview

![image](previews/preview.png)

## Usage

Ouvrir le fichier `rft_variables.php`, précisez votre contenu à corriger (`$content`) et le fichier regex à utiliser (`$lang`), puis ouvrir `rft_testing_page.php`.

## Ajouter

Vous pouvez ajouter des remplacements dans l'array `$regex` dans les fichiers (`fr.php`, `en.php`, etc.) situés dans le dossier `regex/`.

## Références

Quelques liens pour les noms de glyphes et leurs codes (il serait peut-être bon de choisir spécifiquement une seule
source à laquelle se référer ?) :

- http://www.fileformat.info/info/unicode/char/search.htm
- https://en.wikipedia.org/wiki/List_of_Unicode_characters
- http://unicode.org/
- http://unicode-table.com/en/

## Unicode, decimal, hexadecimal, html ?

Il est sûrement possible d'appeller les glyphes à remplacer de différentes manières (http://www.w3schools.com/charsets/ref_utf_misc_symbols.asp). À voir, sinon on peut faire un tableau multi et adapter la fonction de remplacement (ce qui peut servir dans d'autres situations où le html ne fonctionne pas par exemple) ?

Tableau simple :

    $match = array(
	"/oe/"	    =>	    "&#339;",	    # oe => oe lig
    )

Tableau multi :

    $match = array(
	"/oe/"	    =>	array(	    # oe => oe lig
	    "unicode" => "U+0152",
	    "decimal" => "&#339;",
	    "html" => "&oelig;"
	    ),
	...
	)


(Pour l'instant on peut partir sur un tableau simple).  
(Il est possible de récupérer les tableaux xml avec les valeurs correspondantes).

## Notes diverses

### Espace fine insécable

L'espace fine insécable semble ne pas être supportée partout. ~~Il est possible d'utiliser une espace
fine entourée d'une balise span avec un nowrap (quelques infos et solutions ici : https://stackoverflow.com/questions/595365/how-to-render-narrow-non-breaking-spaces-in-html-for-windows).~~

~~    <span style="white-space:nowrap">&thinsp;</span>~~  

Il a plutôt été choisi d'utiliser une espace insécable entourée d'un span avec une class spécifique. Cela permet d'éviter les conflits avec d'autres recherches et de gérer au plus précisément la largeur de l'espace fine en css en utilisant la propriété letter-spacing et en donnant la valeur en em.

    <span class="thinsp">&nbsp;</span>

### Gestion des drapeaux

Un regex recherche tout les mots de une et deux lettres et remplace l'espace qui suit par une espace insécable me manière à ce que celui-ci se retrouve forcément à la ligne s'il est en fin de ligne.
