<?php

    $regex = array(

    	# input		        # output	                              # comment

                                                                # On commence par retirer tout les espaces avant de mettre les espaces fines
    	"/ ;/"	  =>	    ';',	                                  # espace point-virgule		            => point-virgule
    	"/« /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
    	"/ »/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
    	"/ :/"	  =>	    ':',	                                  # espace deux points	                => deux points
    	"/ !/"	  =>	    '!',	                                  # espace point d'exclamation	        => point d'exclamation
    	"/ \?/"	  =>	    '?',	                                  # espace point d'interrogation	      => point d'interrogation
                                                                # On rajoute une balise .thinsp entourant une espace insécable.
                                                                # La gestion de la largeur de l'espace se fait en css avec la propriété letter-spacing
    	"/;/"	    =>	    '<span class="thinsp">&nbsp;</span>;',  # point virgule                       => narrow non breaking space point virgule
    	"/:/"	    =>	    '<span class="thinsp">&nbsp;</span>:',	# deux points	                        => narrow non breaking space deux points
    	"/«/"	    =>	    '«<span class="thinsp">&nbsp;</span>',	# guillement ouvrant                  => guillemet ouvrante narrow non breaking space
    	"/»/"	    =>	    '<span class="thinsp">&nbsp;</span>»',	# guillement fermant                  => narrow non breaking space  guillemet fermante
    	"/!/"	    =>	    '<span class="thinsp">&nbsp;</span>!',	# point d'exclamation                 => narrow non breaking space point d'exclamation
    	"/\?/"	  =>	    '<span class="thinsp">&nbsp;</span>?',	# point d'interrogation               => narrow non breaking space  guillemet fermante
                                                                # Caractères spéciaux
    	"/& /"	  =>	    '&amp; ',	                              # &			                             => &amp;
    	"/oe/"	  =>	    '&#339;',	                              # oe			                           => œ
    	"/ae/"	  =>	    '&#230;',	                              # ae			                           => æ
    	"/OE/"	  =>	    '&#338;',	                              # OE			                           => Œ
    	"/AE/"	  =>	    '&#198;',	                              # AE			                           => Æ
    	"/\.\.\./"	  =>	    '&#8230;',	                            # AE			                           => Æ
    	//"/-/"	    =>	    '&#8209;',	                          # tiret			                         => tiret insécable
                                                                # Espace insécable après les mots de 1 et 2 lettre pour le drapeau
      "/ (\b\p{L}{1,2}\b) /u"	      =>	    ' $1&nbsp;',        # cherche tout les mots de 1 et 2 lettre, accentués compris
      "/&nbsp;(\b[a-z]{1,2}\b) /i"	=>	    '&nbsp;$1&nbsp;'    # dans le cas où 2 mots de 1 ou 2 lettres se suivent
								# Correction des bugs avec les urls
	'/http<span class="thinsp">&nbsp;<\/span>:/' => 'http:', # http:
      '/https<span class="thinsp">&nbsp;<\/span>:/' => 'https:',	# https:
      '/href="mailto<span class="thinsp">&nbsp;<\/span>:/' => 'href="mailto:' # mailto:
    );

?>
