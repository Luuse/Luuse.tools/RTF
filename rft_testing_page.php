<html>
   
     <head>
    
	<!-- inc --> 
	<?php include "inc/meta.php"; ?>
	<?php include "inc/title.php"; ?>
	
	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet/less" type="text/css" href="css/styles.less" />
	
	<!-- fonts -->
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
	
	<!-- lib -->
	<script src="lib/less.min.js" type="text/javascript"></script>
	<script src="lib/jquery-2.1.4.min.js" type="text/javascript"></script>

    </head>
    
    <body>

	<?php 
	    # include the function	
	    include "rft_function.php";
	    
	    # include the variables	
	    include "rft_variables.php";
	?>

	<div id="input" class="content">
	    <h1>input</h1>

	    <?php 
		# echo your content
		echo $content;
	    ?>	
	
	</div>

	<div id="output" class="content">
	    <h1>output</h1>
	    
	    <?php
		# rft and echo your content
		rft($content, $lang);		
	    ?>
	
	</div>

	

	<div id="nav">

	    <button id="correctionShowHideBtn">Show</button>

	</div>	

    </body>

    <!-- js -->
    <script type="text/javascript" src="js/script.js"></script>

</html>
